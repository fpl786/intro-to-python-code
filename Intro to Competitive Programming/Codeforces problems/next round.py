#Next Round problem code forces
#https://codeforces.com/problemset/problem/158/A

nk = input().split()
n = int(nk[0])
k = int(nk[1])

scores = [int(i) for i in input().split()]

x = scores[k-1]
count = 0

for i in scores:
    if i >= x and i > 0:
        count+=1
        
print(count)

#Bela problem kattis
#https://open.kattis.com/problems/bela
l = input().split()

n = int(l[0])*4

dominant = l[1]


d = {
    "A": 11,
    "K": 4,
    "Q": 3,
    "J": 20,
    "T": 10,
    "9": 14,
    "8": 0,
    "7": 0

    }

nd = {
    "A": 11,
    "K": 4,
    "Q": 3,
    "J": 2,
    "T": 10,
    "9": 0,
    "8": 0,
    "7": 0
}


points = 0

for i in range(n):
    x = input()
    if x[1] == dominant:
        points += d[x[0]]
    else:
        points += nd[x[0]]

print(points)

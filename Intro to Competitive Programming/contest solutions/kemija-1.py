#Kemija problem solution using while loop
#https://open.kattis.com/problems/kemija
code = input()
"hepe"
sentence = ""
i = 0
while i < len(code):
    sentence += code[i]
    if code[i] == "a" or code[i] == "e" or code[i] == "i" or code[i] == "o" or code[i] == "u":
        i+=2
    i+=1
        
print(sentence)

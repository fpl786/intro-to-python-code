'''
write a program that takes a list of numbers as input on the same line
print the count of the numbers strictly greater than 5

Eg.

Input:
3 4 5 12 3 1 2
Output:
1

'''

numbers = [int(i) for i in input().split()]

count = 0
for i in numbers:
    if i > 5:
        count +=1

print (count)




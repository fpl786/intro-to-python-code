print("Enter String: ", end="")
text = input()
vowels = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']

for c in vowels:
    text = text.replace(c, "")


print(text)

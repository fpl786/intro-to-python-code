'''

The following is a points table for each
word:

soccer -> 10 points
basketball -> 15 points
hockey -> 30 points
hello-> 10 points
hi -> 5 points


Input: First line will contain a number n. You will then take n words as input

Ouput:
total all the points accumulated by the user


Eg.

Input:
3
soccer
basketball
hockey

Output:
55
'''


points = {
    "soccer":10,
    "basketball": 15,
    "hockey": 30,
    "hello":10,
    "hi": 5}

n = int(input())
score = 0
for i in range(n):
    score += points[input()]

print(score)


'''
fill color and pencolor
'''


import turtle

t = turtle.Turtle()
t.fillcolor("green")
t.pencolor("blue")
t.begin_fill()

for i in range(4):
    t.forward(100)
    t.right(90)
    
t.end_fill()

t.penup()
t.backward(200)
t.pendown()


t.fillcolor("red")

t.begin_fill()

t.circle(40)

t.end_fill()


t.penup()
t.forward(100)
t.pendown()

t.dot(30)

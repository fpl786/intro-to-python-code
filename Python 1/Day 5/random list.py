'''
Randomly generate a list of 10 numbers
from 1 to 100 and print the list

'''
import random

numbers = []


for i in range(10):
    numbers.append(random.randint(1,100))


print(numbers)
    

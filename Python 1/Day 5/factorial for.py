'''
The following program takes a number as input and
calculates the factorial of the number

USE A FOR LOOP
'''


number = int(input("Enter a number: "))



factorial = 1

for i in range(2, number+1):
    factorial *= i


print("Factorial of " + str(number)+ ": "+ str(factorial))

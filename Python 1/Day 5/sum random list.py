'''
Randomly generate a list of 10 numbers
from 1 to 100 and print the list

Add up all the elements and
print the sum

'''


import random

numbers = []


for i in range(10):
    numbers.append(random.randint(1,100))


print(numbers)

s = 0

for i in numbers:
    s+=i

print(s)

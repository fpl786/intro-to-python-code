'''
Use the following list of numbers:

numbers = [5,6,7,8,1,3,46]

Add up all the elements and

print the sum

'''

numbers = [5,6,7,8,1,3,46]

sumNums = 0

for i in numbers:
    sumNums += i
print("Sum: "+ str(sumNums))

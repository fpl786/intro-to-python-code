'''

Implement the guessing game that we made previously
this time give the user 10 guesses. After each guess
print "Too High" if the guess is too high and
print "Too Low" if the guess is too low
print "Correct" if the guess is correct
after 10 guesses print "You Lost! The answer is ___"

MUST USE A FOR LOOP


Look up break statement in python

'''
import random

r = random.randint(1,50)


for i in range(10):
    guess = int(input("Guess the number: "))
    if guess < r:
        print("Too Low")
    elif guess > r:
        print("Too High")
    else:
        print("Correct! You Win")
        break
        
    


if guess != r:
    print("You Lose! The correct number is: " + str(r))

'''
The following program takes a number as input and
calculates the factorial of the number

USE A WHILE LOOP
'''

number = int(input("Enter a number: "))

counter = 2
factorial = 1


while counter <= number:
    
    #factorial = factorial * counter
    
    factorial *= counter
    counter += 1
    
print("Factorial of " + str(number)+ ": "+ str(factorial))

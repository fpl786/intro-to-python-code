import random


num = random.randint(-20,20)


print("Random Number: " + str(num))

if num > 0:
    print("positive")
elif num < 0:
    print("negative")
else:
    print("zero")


if num % 2 == 0:
    print("even")
else:
    print("odd")

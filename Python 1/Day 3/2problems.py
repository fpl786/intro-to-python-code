'''
Problem 1:
take in a number is input
print "positive" if the number is positive
and "negative" if the number is negative
if the number is 0 than print "zero"
'''


'''
Problem 2:
take in a number as input
print "even" if the number is even
and "odd" if the number is odd
HINT: Use the % operator
'''

num  = int(input())



if num > 0:
    print("positive")
elif num < 0:
    print("negative")
else:
    print("zero")


if num % 2 == 0:
    print("even")
else:
    print("odd")

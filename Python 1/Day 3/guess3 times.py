'''
Build off of the previous program and give the user 3 guesses
if all 3 guesses are incorrect, print the correct answer at the end
of the 3 guesses

if the user is correct print correct and exit the program

Eg. 1

Guess the number: 5
Try Again
Guess the number: 10
Try Again
Guess the number: 3
Incorrect! The answer is 7


Eg. 2

Guess the number: 9
Try Again
Guess the number: 4
Correct!

'''

import random

num = random.randint(1,20)


guess = int(input("Guess the number"))

if guess != num:
    print("Try Again")
    guess = int(input("Guess the number"))
    if guess!=num:
        print("Try Again")
        guess = int(input("Guess the number"))
        if guess!=num:
            print("Incorrect! The number is " + str(num))
        else:
            print("Correct!")
    else:
        print("Correct!")
else:
    print("Correct!")
    
    

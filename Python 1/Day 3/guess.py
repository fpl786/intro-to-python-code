'''
using the random module,
generate a random number from 1 to 20

ask the user for to guess the number
if the number is correct print "Correct"
otherwise print "Incorrect. The answer is __"
'''
import random

num = random.randint(1,20)

guess = int(input("Guess the number: "))

if guess == num:
    print("Correct")
else:
    print("Incorrect! The answer is "+ str(num))

'''
print all numbers from 1 to 20
except for 1, 5 , 9 , 12, and 16
You MUST use a while loop
'''



counter = 1

while counter <= 20:
    if counter != 1 and counter != 5 and counter != 9 and counter != 12 and counter != 16:
        print(counter)
    counter += 1

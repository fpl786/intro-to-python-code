'''
Ask the user for the base.
Then ask for the exponent

calculate the power

Eg.

Enter the base
5
Enter the exponent
3

The answer is 125

'''


base = int(input("Enter the base: "))
exponent = int(input("Enter the exponent: "))

print("The answer is " + str(base**exponent))

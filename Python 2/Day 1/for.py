
numbers = [5,6,8,3,2,4,6,4]

print("For Loop 1")

#for loop over a list
for i in numbers:
    print(i)

print("While Loop 1")

j = 0
while j < len(numbers):
    print(numbers[j])
    j+=1
    
print("For Loop 2")
#for loop over a range    
for counter in range(5):
    print(counter)


print("While loop 2")
counter = 0
while counter < 5:
    print(counter)
    counter+=1

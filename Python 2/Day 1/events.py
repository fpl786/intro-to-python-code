'''
modify this so that when you right click
on the screen, the background, fill color and pen color
will change to a random color

HINT: Use the random.choice function

'''

import turtle
import random 

t = turtle.Turtle()
colors = ["yellow", "red", "orange", "blue", "pink", "purple"]

def up():
    t.setheading(90)
    t.forward(100)


def down():
    t.setheading(270)
    t.forward(100)

def right():
    t.setheading(0)
    t.forward(100)

def left():
    t.setheading(180)
    t.forward(100)

def rotate(x,y):
    t.left(360)

def change_color(x,y):
    turtle.bgcolor(random.choice(colors))
    t.pencolor(random.choice(colors))
    t.fillcolor(random.choice(colors))
    
turtle.bgcolor("black")
t.speed(10)

t.shape("turtle")
t.pencolor("blue")
t.fillcolor("deep pink")
t.pensize(5)
t.shapesize(3,3,3)



turtle.listen()

turtle.onkey(up,"Up")
turtle.onkey(down, "Down")
turtle.onkey(left, "Left")
turtle.onkey(right, "Right")

turtle.onscreenclick(rotate,1)

turtle.onscreenclick(change_color,2)


turtle.mainloop()

'''
Ask the user to type a word
Check if the word is a palindrome

Eg.
racecar

If the word is a palindrome, print "Your word is a palindrome"

Otherwise print "Not a palindrome"
'''


word = input()

if word == word[::-1]:
    print(word+ " is a palindrome")
else:
    print(word+ " is not a palindrome")
    

'''

Take in a pen color and fill color as input
draw a square with those colors

Take in another pen color and fill color as input
draw a circle with those colors. Draw the circle in a different location

'''


import turtle


penSquare = input("Enter pen colour of square: ")
fillSquare = input("Enter fill colour of square: ")

penCircle = input("Enter pen colour of circle: ")
fillCircle = input("Enter fill colour of circle: ")

t = turtle.Turtle()

t.shape("turtle")

t.pencolor(penSquare)
t.fillcolor(fillSquare)
t.pensize(10)

t.begin_fill()
for i in range(4):
    t.forward(100)
    t.right(90) 

t.end_fill()

t.penup()
t.backward(200)
t.pendown()

t.pencolor(penCircle)
t.fillcolor(fillCircle)

t.begin_fill()
t.circle(50)
t.end_fill()    




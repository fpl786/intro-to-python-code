'''
generate a list of 10 random numbers from 1 to 50
ask the user to guess a number
check if the number is in the list or not


if the number is in the list - print you win
otherwise print - you lose

'''

import random

numbers = []

for i in range(10):
    numbers.append(random.randint(1,50))


guess = int(input("Guess a number"))  


found = False

for i in numbers:
    if i == guess:
        found = True

if found:
    print("You Win!")
else:
    print("You Lose!")

print("Winning numbers: ", end = "")
for i in numbers:
    print(i, end = " ")
       
    
        

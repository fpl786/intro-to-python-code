'''
generate a random animal from animals.txt
print a dash for every letter in the animals name

modify this program to include a function.
The function takes in 1 argument - the animals name - and
print the appropriate number of dashes

'''


import random

def print_dashes(a):
    for i in a:
        print("-", end = " ")



file = open("animals.txt", "r")

animals = file.readlines()

animal = random.choice(animals).strip()

print(animal)

print_dashes(animal)



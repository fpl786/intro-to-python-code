#lottery program shortcut using the "in" keyword

import random

numbers = []

for i in range(10):
    numbers.append(random.randint(1,50))

print(numbers)
guess = int(input("Guess a number"))


if guess in numbers:
    print("You Win!")
else:
    print("You Lose!")

print("Winning numbers: ", end = "")
for i in numbers:
    print(i, end = " ")
          
    

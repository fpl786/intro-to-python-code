'''
using the animals.txt file generate
a random animal and print it to the screen

HINT: use file.readlines()

'''
import random

file = open("animals.txt", "r")
animals = file.readlines()
file.close()
animal = random.choice(animals)

print(animal.strip())

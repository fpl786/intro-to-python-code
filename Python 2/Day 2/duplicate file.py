'''
Using the hello.txt file you created, write a
program that reads from that file and copies it
into a new file called hello-1.txt

Two methods: file.readline() and file.read()

'''

file = open("hello.txt", "r")

#text = file.readlines()
text1 = file.read()

file.close()


file = open("hello-2.txt", "w")

'''
for line in text:
    file.write(line)
'''

print(text1)

file.close()

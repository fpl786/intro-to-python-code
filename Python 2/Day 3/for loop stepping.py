print("Regular For Loop")
#regular for loop
for i in range(4, 25):
    print(i)

print("For loop with stepping")
#for loop with stepping
for i in range(4,25, 2):
    print(i)

print("counting backwards")
#counting backwards
for i in range(25,3, -1):
    print(i)

'''
write a python turtle program that allows the user
to click anywhere on the screen and moves the turtle
to that specific location on the screen

HINT: use t.goto
'''
import turtle


t= turtle.Turtle()
t.speed(30)

    
turtle.listen()

turtle.onscreenclick(t.goto, 1)

turtle.mainloop()

'''
using the animals.txt file, build a guessing game

Ask the user to guess an animal,
print "Correct" if the user is correct
print "Try Again" if the user is incorrect

Give the user a maximum of 5 guesses

'''

import random

file = open("animals.txt", "r")

animals = file.readlines()

animal = random.choice(animals).strip()
print(animal)

for i in range(5):
    guess = input("Guess the animal: ")
    if guess == animal:
        print("Correct")
        break
    elif i != 4:
        print("Try Again")
    else:
        print("Sorry you lost!\n The correct answer is "+animal) 





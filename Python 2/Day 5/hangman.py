import turtle
import random

file = open("animals.txt", "r")
animals = file.readlines()
file.close()

word = random.choice(animals).strip()
print(word)

t = turtle.Turtle()

t.hideturtle()
t.penup()
t.goto(-250,200)

t.pendown()
t.backward(50)
t.right(90)
t.forward(200)

t.right(90)
t.forward(30)
t.backward(60)

t.penup()
t.goto(-250, 200)
t.pendown()
t.left(90)
t.forward(20)
t.right(90)


#head
def head():
    t.circle(20)

#body
def body():
    t.left(90)
    t.penup()
    t.forward(40)
    t.pendown()
    t.forward(100)

#right arm
def right_arm():
    t.backward(80)
    t.right(45)
    t.forward(30)
    t.backward(30)

#left arm
def left_arm():
    t.left(90)
    t.forward(30)
    t.backward(30)
    t.right(45)
    t.forward(80)

#right leg
def right_leg():
    t.right(45)
    t.forward(30)
    t.backward(30)

#left leg
def left_leg():
    t.left(90)
    t.forward(30)

'''
create a second turtle object
generate a random animal from animals.txt
for every letter in the animal's name
draw a dash on the screen. Each dash should be
50 px long with 10px of space between each dash
'''

t2 = turtle.Turtle()


for i in word:
    t2.pendown()
    t2.forward(50)
    t2.penup()
    t2.forward(10)

'''
Figure out how to deal with incorrect guesses.
1 time:  draw the head
2 times: body
3 times: right arm
4 times: left arm
5 times: right leg
6 times: left leg
'''

incorrect_guesses = 0
correct_guesses = 0

guesses = []

while (incorrect_guesses < 6) and (correct_guesses < len(word)):
    
    guess = turtle.textinput("Hangman", "Guess a letter:")

    if (guess not in word) and (guess not in guesses):
        incorrect_guesses+=1
        if incorrect_guesses == 1:
            head()
        elif incorrect_guesses == 2:
            body()
        elif incorrect_guesses == 3:
            right_arm()
        elif incorrect_guesses == 4:
            left_arm()
        elif incorrect_guesses == 5:
            right_leg()
        elif incorrect_guesses == 6:
            left_leg()

        t2.penup()
        t2.goto(60*(incorrect_guesses-1)+25, -50)
        t2.pencolor("red")
        t2.pendown()
        
        t2.write(guess, False, align = 'center', font = ('Arial', 30))
        t2.penup()

        
    else:

        if guess not in guesses:
            for i in range(len(word)):
                if guess == word[i]:
                    correct_guesses+=1
                    t2.pencolor("black")
                    t2.penup()
                    t2.goto(60*i+25, 10)
                    t2.pendown()
                    t2.write(guess, False, align = 'center', font = ('Arial', 30))
                    
    if guess not in guesses:
        guesses += guess



t2.penup()
t2.home()
t2.pencolor("Black")

turtle.clearscreen()

turtle.bgcolor("orange")

if incorrect_guesses == 6:
    t2.write("Sorry You Lose!\n Correct Answer: " + word, False, align = "center", font = ("Arial", 30))
else:
    t2.write("Congratulations! You Won!", False, align = "center", font = ("Arial", 30))
    

        

           
    
        
            
    
    
    






